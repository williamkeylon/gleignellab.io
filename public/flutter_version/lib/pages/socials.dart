import 'package:flutter/material.dart';
import 'package:flutter_version/widgets/social_button.dart';

class Socials extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        SocialButton(
          text: 'GitHub',
          url: 'https://github.com/Germain-L',
        ),
        SocialButton(
          text: 'Twitter',
          url: 'https://twitter.com/Germain_lgl',
        ),
        SocialButton(
          text: 'LinkedIn',
          url: 'https://www.linkedin.com/in/germain-leignel-785b61172/',
        ),
      ],
    );
  }
}
