import 'package:flutter/material.dart';
import 'package:flutter_version/pages/socials.dart';

class Home extends StatelessWidget {
  static const TextStyle _textStyle =
      TextStyle(color: Color(0xffEEEEEE), fontSize: 55);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff252934),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RichText(
                text: TextSpan(children: [
                  TextSpan(
                    text: "Hey, I'm ",
                    style: _textStyle,
                  ),
                  TextSpan(
                      text: "Germain Leignel",
                      style: TextStyle(color: Color(0xffE31B6D), fontSize: 55))
                ]),
              ),
              Text(
                "I build apps with flutter,",
                style: _textStyle,
              ),
              Text(
                "and I'm a student at EPSI Bordeaux.",
                style: _textStyle,
              ),
              Socials(),
            ],
          ),
        ],
      ),
    );
  }
}
