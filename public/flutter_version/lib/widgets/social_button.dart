import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SocialButton extends StatefulWidget {
  final String text;
  final String url;

  const SocialButton({Key key, @required this.text, @required this.url})
      : super(key: key);
  @override
  _SocialButtonState createState() => _SocialButtonState();
}

class _SocialButtonState extends State<SocialButton> {
  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Color color = Color(0xff222222);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 25),
      child: FlatButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
            side: BorderSide(color: Colors.transparent)),
        onPressed: () {
          _launchURL(widget.url);
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
          child: Text(
            widget.text,
            style: TextStyle(fontSize: 45, color: color),
          ),
        ),
        focusColor: Color(0xffE31B6D),
        color: Color(0xffEEEEEE),
      ),
    );
  }
}
